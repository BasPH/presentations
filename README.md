# Presentations

## EuroPython 2018 - Edinburgh
[YouTube](https://www.youtube.com/watch?v=KUvB1hqF2OA)
[Slides](https://gitlab.com/BasPH/presentations/blob/master/slides/europython2018.pdf?inline=false)

## Airflow Meetup 2018 - Amsterdam
[YouTube](https://www.youtube.com/watch?v=MM8tfTrcnfk)
[Slides](https://gitlab.com/BasPH/presentations/raw/master/slides/airflow-meetup-2018.pdf?inline=false)
